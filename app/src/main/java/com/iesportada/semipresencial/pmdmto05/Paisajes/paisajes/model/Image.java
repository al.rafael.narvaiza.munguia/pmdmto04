package com.iesportada.semipresencial.pmdmto05.Paisajes.paisajes.model;
/**
 * @author RNarvaiza
 * PMDM TO04 EJ1
 * Image POJO to get file and rating
 */

public class Image {
    String rate, url;

    public void setRate(String rate) {
        this.rate = rate;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getRate() {
        return rate;
    }

    public String getUrl() {
        return url;
    }

    public Image() {
    }

    public Image(String rate, String url) {
        this.rate = rate;
        this.url = url;
    }
}
