package com.iesportada.semipresencial.pmdmto05;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;

import com.iesportada.semipresencial.pmdmto05.Animacion.MainActivity3;
import com.iesportada.semipresencial.pmdmto05.Paisajes.paisajes.UI.MainActivity2;
import com.iesportada.semipresencial.pmdmto05.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {

    private Animation animSequential;
    private ActivityMainBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        binding=ActivityMainBinding.inflate(getLayoutInflater());
        View v = binding.getRoot();
        setContentView(v);
        getSupportActionBar().hide();

        binding.PaisajesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, MainActivity2.class));
            }
        });

        binding.buttonImagenes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, MainActivity3.class));
            }
        });

    }


}