package com.iesportada.semipresencial.pmdmto05.Paisajes.paisajes.View;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.bumptech.glide.Glide;
import com.iesportada.semipresencial.pmdmto05.R;
import com.iesportada.semipresencial.pmdmto05.Paisajes.paisajes.model.Image;

import java.io.InputStream;
import java.net.URL;
import java.util.List;
import java.util.Objects;

/**
 * @author RNarvaiza
 * PMDM TO04 EJ1
 * ViewPagerAdapter to show imageList.
 */

public class ViewPagerAdapter extends PagerAdapter {

    Context contex;
    List<Image> myImageList;
    LayoutInflater layoutInflater;
    EditText inputRate;

    public ViewPagerAdapter(Context context, List<Image> imageList){
        this.contex = context;
        this.myImageList=imageList;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount() {
        return myImageList.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == ((LinearLayout) object);
    }


    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        View itemView = layoutInflater.inflate(R.layout.item_imageview, container, false);
        ImageView img = (ImageView) itemView.findViewById(R.id.imageViewItem);
        new LoadImage(img).execute(myImageList.get(position).getUrl());
        Objects.requireNonNull(container).addView(itemView);
        inputRate = itemView.findViewById(R.id.editTextNumber);

        if(inputRate.getText().toString().length()==0 | inputRate.getText().toString().isEmpty()){
            myImageList.get(position).setRate(inputRate.getText().toString());
        }
        else{
            inputRate.setText(myImageList.get(position).getRate());
        }
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {

        container.removeView((LinearLayout) object);
    }

    private class LoadImage extends AsyncTask<String, String, Bitmap> {
        ImageView img=null;
        public LoadImage(ImageView img){
            this.img=img;
        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }
        protected Bitmap doInBackground(String... args) {
            Bitmap bitmap=null;
            try {
                bitmap = BitmapFactory.decodeStream((InputStream)new URL(args[0]).getContent());
            } catch (Exception e) {
                e.printStackTrace();
            }
            return bitmap;
        }
        protected void onPostExecute(Bitmap image) {
            if(image != null){
                img.setImageBitmap(image);
            }
        }
    }
}
