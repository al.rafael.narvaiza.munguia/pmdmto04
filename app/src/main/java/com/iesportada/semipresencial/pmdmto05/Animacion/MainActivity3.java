package com.iesportada.semipresencial.pmdmto05.Animacion;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.iesportada.semipresencial.pmdmto05.R;
import com.iesportada.semipresencial.pmdmto05.databinding.ActivityMain3Binding;

import org.jetbrains.annotations.NotNull;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import okhttp3.*;
import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

/**
 * @author RNarvaiza
 * PMDM TO04 EJ2
 * Read an url list with images, trying to load it on an imageView with Glide with animation between pics. Catch errors on errores.txt on SDcard route /Download.
 */


public class MainActivity3 extends AppCompatActivity {

    public static final String URL = "http://165.227.158.59/files/file.txt";

    List<String> cleanUrls = new ArrayList<>();
    private ActivityMain3Binding binding;
    private static final int REQUEST_WRITE = 1;
    String stringLogger = "";

    public MainActivity3() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);
        binding = ActivityMain3Binding.inflate(getLayoutInflater());
        View v = binding.getRoot();
        setContentView(v);

        flushFile(); //Before app execution, file will be erased in order to get only one log per execution.

        OkHTTPDownload(); //Calling OkHTTP to download the content of files.txt

    }


    private void OkHTTPDownload() {
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(URL)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                SimpleDateFormat sdf = new SimpleDateFormat("MMM dd,yyyy HH:mm");
                StringBuilder stringBuilder = new StringBuilder();
                String message = "\n\n No es posible realizar la conexión con okhtttp: " + e.getMessage() + "\n en la URL : " + URL + "Fecha y hora del error: " + sdf.format(System.currentTimeMillis());
                stringBuilder.append(message);
                System.out.println(message); //Printing error on console.
                save(stringBuilder.toString()); //Attaching a new string to be written to errores.txt
                showToast(message); //Printing error on toast.
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {

                try(ResponseBody responseBody = response.body()){
                    if (!response.isSuccessful()){
                        throw new IOException("Unexpected code " + response);
                    }else{
                        String message = response.body().string(); //Getting the full content of files.txt
                        String[] urls = message.split("\r\n"); //Splitting the whole downloaded string into spared strings corresponding to each url.
                        setCleanUrls(urlFilter(urls));

                        // TODO: Achieve cross animations between imageViews.
                        Handler handler = new Handler(Looper.getMainLooper());
                        handler.postDelayed(new Runnable() { //New thread to handle the url loading execution on imageViews.
                            @Override
                            public void run() {
                                for (int i = 0; i<getCleanUrls().size(); i++){
                                    int finalI = i;
                                    switch (finalI){
                                        case 0:
                                            binding.imageView1.setVisibility(View.VISIBLE);
                                            loadImage(getCleanUrls().get(finalI), binding.imageView1);
                                            break;
                                        case 1:
                                            binding.imageView1.setVisibility(View.INVISIBLE);
                                            binding.imageView2.setVisibility(View.VISIBLE);
                                            loadImage(getCleanUrls().get(finalI), binding.imageView2);
                                            break;
                                        case 2:
                                            binding.imageView2.setVisibility(View.INVISIBLE);
                                            binding.imageView3.setVisibility(View.VISIBLE);
                                            loadImage(getCleanUrls().get(finalI), binding.imageView3);
                                            break;
                                        case 3:
                                            binding.imageView3.setVisibility(View.INVISIBLE);
                                            binding.imageView4.setVisibility(View.VISIBLE);
                                            loadImage(getCleanUrls().get(finalI), binding.imageView4);
                                            break;
                                        default:
                                            loadImage(getCleanUrls().get(finalI), binding.imageView4);
                                    }
                                }
                                save(getStringLogger());
                            }
                        }, 2000);

                        //Other way to achieve the same unsuccessful goal.
                        /*
                        MainActivity3.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                for (int i = 0; i<getCleanUrls().size(); i++){
                                    int finalI = i;

                                    switch (finalI){
                                        case 0:
                                            binding.imageView1.setVisibility(View.VISIBLE);
                                            loadImage(getCleanUrls().get(finalI), binding.imageView1);
                                            break;
                                        case 1:
                                            binding.imageView1.setVisibility(View.INVISIBLE);
                                            binding.imageView2.setVisibility(View.VISIBLE);
                                            loadImage(getCleanUrls().get(finalI), binding.imageView2);
                                            break;
                                        case 2:
                                            binding.imageView2.setVisibility(View.INVISIBLE);
                                            binding.imageView3.setVisibility(View.VISIBLE);
                                            loadImage(getCleanUrls().get(finalI), binding.imageView3);
                                            break;
                                        case 3:
                                            binding.imageView3.setVisibility(View.INVISIBLE);
                                            binding.imageView4.setVisibility(View.VISIBLE);
                                            loadImage(getCleanUrls().get(finalI), binding.imageView4);
                                            break;
                                        default:
                                            loadImage(getCleanUrls().get(finalI), binding.imageView4);
                                    }
                                    try {
                                        Thread.sleep(1000);
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }
                                }
                                save(getStringLogger());
                            }
                        });

                         */

                    }
                }
            }
        });

    }

    /**
     * Constructor designed to load a link inside an especific imageView and handle all their exceptions.
     * @param imageLink
     * @param imageView
     */

    private void loadImage(String imageLink, ImageView imageView){
        Glide
                .with(getApplicationContext())
                .load(imageLink)
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        SimpleDateFormat sdf = new SimpleDateFormat("MMM dd,yyyy HH:mm");
                        StringBuilder stringBuilder = new StringBuilder();
                        String message = "\n\n Error en la carga de la foto " + e.getMessage() + "\n en la URL : " + imageLink + " Fecha y hora del error: " + sdf.format(System.currentTimeMillis());
                        stringBuilder.append(message);
                        System.out.println(message);
                        save(stringBuilder.toString());

                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        return false;
                    }
                })
                .into(imageView);
    }

    /**
     * Constructor to avoid malformed URL's.
     * @param urlArray
     * @return
     */

    private List<String> urlFilter(String[] urlArray){
        List<String> cleanedUrl = new ArrayList<>();
        String message = null;
        for(int i = 0 ; i < urlArray.length; i++){
            if (!urlArray[i].matches("((\\w){4,5})((\\W){3})(\\w*)(\\.)(\\w*)(\\W)(.*)(\\.)((\\w){3})")){
                SimpleDateFormat sdf = new SimpleDateFormat("MMM dd,yyyy HH:mm");
                message = "\n La url : " + urlArray[i] + " no está bien construida." + " Fecha y hora del error: " + sdf.format(System.currentTimeMillis());
                System.out.println(message);
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append(message);
                save(stringBuilder.toString());
            }
            else {
                if (urlArray.length<0){
                    message = "\n\n No hay ninguna URL válida " + "Fecha y hora del error: " + System.currentTimeMillis();
                    System.out.println(message);
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.append(message);
                    save(stringBuilder.toString());
                }
                else {
                    cleanedUrl.add(urlArray[i]);
                }
            }
        }
        return cleanedUrl;
    }

    /**
     * Function to get Write permission on SD.
     * @param s
     */

    public void save(String s) {

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        String permissions = Manifest.permission.WRITE_EXTERNAL_STORAGE;

        if (ActivityCompat.checkSelfPermission(this, permissions) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this, new String[]{permissions}, REQUEST_WRITE);

        } else {

            if (!enableWrite()){

            }
            else{
                write(s);
            }
        }
    }

    /**
     * Method to check if SD is mounted.
     * @return
     */

    public static boolean enableWrite() {
        boolean write = false;
        String status = Environment.getExternalStorageState();
        if(status.equals(Environment.MEDIA_MOUNTED)){
            write = true;
        }
        return write;
    }

    /**
     * Method to clear errores.txt before each log execution.
     */

    public void flushFile(){
        File sdCard = Environment.getExternalStorageDirectory();
        @SuppressLint("DefaultLocale") String fileName = String.format("errores.txt");
        File dir = new File(sdCard.getAbsolutePath() + "/Download/");

        if(!dir.exists()){
            dir.mkdirs();
        }

        final File myTxtFile = new File(dir, fileName);

        try (PrintWriter pw = new PrintWriter(myTxtFile);)
        {
            pw.print("");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * Simply try with resources writting method that allow to input several strings from different sources
     * @param s
     * @return
     */

    public boolean write(String s){

        boolean correcto = false;

        File sdCard = Environment.getExternalStorageDirectory();
        @SuppressLint("DefaultLocale") String fileName = String.format("errores.txt");
        File dir = new File(sdCard.getAbsolutePath() + "/Download/");

        if(!dir.exists()){
            dir.mkdirs();
        }

        final File myTxtFile = new File(dir, fileName);


        try
                (FileOutputStream fos = new FileOutputStream(myTxtFile, true);
                 OutputStreamWriter osw = new OutputStreamWriter(fos);
                 BufferedWriter bw = new BufferedWriter(osw))
        {
            bw.append(s);
            bw.flush();
            correcto = true;
        } catch (FileNotFoundException  e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
        return correcto;
    }



    public void showToast(String message){
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
    }

    public List<String> getCleanUrls() {
        return cleanUrls;
    }

    public void setCleanUrls(List<String> cleanUrls) {
        this.cleanUrls = cleanUrls;
    }

    public String getStringLogger() {
        return stringLogger;
    }

    public void setStringLogger(String stringLogger) {
        this.stringLogger = stringLogger;
    }
}