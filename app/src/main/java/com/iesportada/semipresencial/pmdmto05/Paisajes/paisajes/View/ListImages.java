package com.iesportada.semipresencial.pmdmto05.Paisajes.paisajes.View;


import com.iesportada.semipresencial.pmdmto05.Paisajes.paisajes.model.Image;

import java.util.ArrayList;
import java.util.List;

/**
 * @author RNarvaiza
 * PMDM TO04 EJ1
 * Populate a list of Image
 */

public class ListImages {
    public static List<Image> imageList(){
        Image image;
        List<Image> myImages = new ArrayList<>();
        myImages.add(new Image("", "https://i.imgur.com/MOziuEi.png"));
        myImages.add(new Image("", "https://i.imgur.com/qSzjrqm.png"));
        myImages.add(new Image("", "https://i.imgur.com/nZt37gV.png"));
        myImages.add(new Image("", "https://i.imgur.com/65G8EAh.png"));
        myImages.add(new Image("", "https://i.imgur.com/Zknawnv.png"));
        myImages.add(new Image("", "https://i.imgur.com/qSBarv8.png"));

        return myImages;
    }
}
