package com.iesportada.semipresencial.pmdmto05.Paisajes.paisajes.UI;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.iesportada.semipresencial.pmdmto05.R;
import com.iesportada.semipresencial.pmdmto05.Paisajes.paisajes.View.ListImages;
import com.iesportada.semipresencial.pmdmto05.Paisajes.paisajes.View.ViewPagerAdapter;
import com.iesportada.semipresencial.pmdmto05.databinding.ActivityMain2Binding;

/**
 * @author RNarvaiza
 * PMDM TO04 EJ1
 * Load an animation before an slide image collection.
 */

public class MainActivity2 extends AppCompatActivity {

    private ActivityMain2Binding binding;
    Animation sequential;
    ViewPagerAdapter viewPagerAdapter;
    ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        binding = ActivityMain2Binding.inflate(getLayoutInflater());
        View v = binding.getRoot();
        setContentView(v);

        textViewAnimation();

    }

    /**
     * Simply animation with two different animations designed on same xml source.
     */

    private void textViewAnimation() {

                sequential = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.sequential);
                binding.textViewValoracion.setVisibility(View.VISIBLE);
                binding.textViewValoracion.startAnimation(sequential);
                new Handler().postDelayed(()->{
                    binding.textViewValoracion.clearAnimation();
                    pagerInit();
                }, 6000);

    }

    /**
     * PageView initialization.
     */

    private void pagerInit() {
        binding.textViewValoracion.setVisibility(View.GONE);
        viewPager = binding.viewpager;
        viewPagerAdapter = new ViewPagerAdapter(MainActivity2.this,ListImages.imageList());
        viewPager.setAdapter(viewPagerAdapter);
        onSwipeListener();
    }

    private void onSwipeListener() {
        ViewPager.OnPageChangeListener swipeListener = new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}

            @Override
            public void onPageSelected(int position) {
                MediaPlayer mPlayer = MediaPlayer.create(getApplicationContext(), R.raw.swipe);
                mPlayer.start();
            }

            @Override
            public void onPageScrollStateChanged(int state) {}
        };
        viewPager.addOnPageChangeListener(swipeListener);
    }


}